﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CollectionsAndLinqConsoleApp.Models
{
	public class User
	{
		public int Id { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Email { get; set; }
		public DateTime Birthday { get; set; }
		public DateTime RegisteredAt { get; set; }
		public int? TeamId { get; set; }

		public Team Team { get; set; }
		public IEnumerable<Task> Tasks { get; set; }
		public IEnumerable<Project> Projects { get; set; }

		public User() { }
		public User(User user)
		{
			Id = user.Id;
			FirstName = user.FirstName;
			LastName = user.LastName;
			Email = user.Email;
			Birthday = user.Birthday;
			RegisteredAt = user.RegisteredAt;
			TeamId = user.TeamId;
		}
		public User(User user, Team team) : this(user)
		{
			Team = team;
		}

		public User(User user, IEnumerable<Task> tasks) : this(user)
		{
			Tasks = tasks;
		}

		public User(User user, IEnumerable<Project> projects) : this(user)
		{
			Projects = projects;
		}
	}
}
