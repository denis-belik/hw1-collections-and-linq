﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CollectionsAndLinqConsoleApp.Models
{
	public class TaskStateModel
	{
		public int Id { get; set; }
		public string Value { get; set; }
	}
}
