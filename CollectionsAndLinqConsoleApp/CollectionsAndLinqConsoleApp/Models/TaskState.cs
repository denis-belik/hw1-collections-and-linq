﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CollectionsAndLinqConsoleApp.Models
{
	public enum TaskState
	{
		Created,
		Started,
		Finished,
		Canceled
	}
}
