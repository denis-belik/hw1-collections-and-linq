﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CollectionsAndLinqConsoleApp.Models.ResultModels
{
	public class TeamId_Name_Members
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public IEnumerable<User> Members { get; set; }
	}
}
