﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CollectionsAndLinqConsoleApp.Models.ResultModels
{
	public class User_LatestProject_TasksCount_NotFinishedTasksCount_LongestDurationTask
	{
		public User User { get; set; }
		public Project LatestProject { get; set; }	
		public int? LatestProjectTasksCount { get; set; }	
		public int UsersNotFinishedTasksCount { get; set; }
		public Task UsersLongestDurationTask { get; set; }
	}
}
