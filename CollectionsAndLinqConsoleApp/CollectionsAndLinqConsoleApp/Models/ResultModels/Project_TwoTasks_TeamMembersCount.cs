﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CollectionsAndLinqConsoleApp.Models.ResultModels
{
	public class Project_TwoTasks_TeamMembersCount
	{
		public Project Project { get; set; }
		public Task LongestDescriptionTask { get; set; }
		public Task ShortestNameTask { get; set; }
		public int TeamMembersCount { get; set; }
	}
}
