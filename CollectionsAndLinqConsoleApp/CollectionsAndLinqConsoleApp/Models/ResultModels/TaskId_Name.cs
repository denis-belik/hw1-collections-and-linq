﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CollectionsAndLinqConsoleApp.Models.ResultModels
{
	public class TaskId_Name
	{
		public int Id { get; set; }
		public string Name { get; set; }
	}
}
