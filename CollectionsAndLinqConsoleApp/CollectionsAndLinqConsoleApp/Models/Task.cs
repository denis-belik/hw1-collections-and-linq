﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CollectionsAndLinqConsoleApp.Models
{
	public class Task
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public DateTime CreatedAt { get; set; }
		public DateTime FinishedAt { get; set; }
		public TaskState State { get; set; }
		public int ProjectId { get; set; }
		public int PerformerId { get; set; }

		public User Performer { get; set; }

		public Task() { }
		public Task(Task task, User performer)
		{
			Id = task.Id;
			Name = task.Name;
			Description = task.Description;
			CreatedAt = task.CreatedAt;
			FinishedAt = task.FinishedAt;
			State = task.State;
			ProjectId = task.ProjectId;
			PerformerId = task.PerformerId;

			Performer = performer;
		}
	}
}
