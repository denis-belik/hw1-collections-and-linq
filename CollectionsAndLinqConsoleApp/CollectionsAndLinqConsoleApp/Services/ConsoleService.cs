﻿using CollectionsAndLinqConsoleApp.Models;
using CollectionsAndLinqConsoleApp.Models.ResultModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace CollectionsAndLinqConsoleApp.Services
{
	public static class ConsoleService
	{
		public static void PrintTask1(Dictionary<Project, int> projectTasksCount)
		{
			Console.WriteLine("Task 1:");
			Console.WriteLine("Отримати кiлькiсть таскiв у проектi конкретного користувача (по id) (словник, де key буде проект, а value кiлькiсть таскiв).");
			Console.WriteLine();

			Console.WriteLine("Project id | Tasks count | Author id");
			foreach (var keyValuePair in projectTasksCount)
			{
				Console.WriteLine($"{keyValuePair.Key.Id,10} | {keyValuePair.Value,-11} | {keyValuePair.Key.AuthorId}");
			}

			Console.WriteLine();
		}

		public static void PrintTask2(IEnumerable<Models.Task> tasksByNameLength)
		{
			Console.WriteLine("Task 2:");
			Console.WriteLine("Отримати список таскiв, призначених для конкретного користувача (по id), де name таска <45 символiв (колекцiя з таскiв).");
			Console.WriteLine();

			Console.WriteLine("Task id | Name length | PerformerId | Name");
			foreach (var task in tasksByNameLength)
			{
				Console.WriteLine($"{task.Id,7} {"|"} {task.Name.Length,-11} | {task.PerformerId,-11} | {task.Name}");
			}

			Console.WriteLine();
		}

		public static void PrintTask3(IEnumerable<TaskId_Name> tasksFinishedIn2020)
		{
			Console.WriteLine("Task 3:");
			Console.WriteLine("Отримати список (id, name) з колекцiї таскiв, якi виконанi (finished) в поточному (2020) роцi для конкретного користувача (по id).");
			Console.WriteLine();

			Console.WriteLine("Task id | Task name");
			foreach (var task in tasksFinishedIn2020)
			{
				Console.WriteLine($"{task.Id,7} | {task.Name}");
			}
		}

		public static void PrintTask4(IEnumerable<TeamId_Name_Members> teamsWithMembersOlderThan10Years)
		{
			Console.WriteLine("Task 4:");
			Console.WriteLine("Отримати список (id, iм'я команди i список користувачiв) з колекцiї команд, учасники яких старшi 10 рокiв, вiдсортованих за датою реєстрацiї користувача за спаданням, а також згрупованих по командах.");
			Console.WriteLine();

			Console.WriteLine($"Team id | Team name {"|",3} Team members list");
			foreach (var team in teamsWithMembersOlderThan10Years)
			{
				Console.WriteLine($"{team.Id,7} | {team.Name,-11} | Members:");

				foreach (var member in team.Members)
				{
					Console.WriteLine($"{"|",9} {"|",13} {member.Id,3}, {member.FirstName,10}, {DateTime.Now.Year - member.Birthday.Year,-2} years old, registered {member.RegisteredAt}");
				}
			}
		}

		public static void PrintTask5(IEnumerable<User> orderedUsers)
		{
			Console.WriteLine("Task 5:");
			Console.WriteLine("Отримати список користувачiв за алфавiтом first_name (по зростанню) з вiдсортованими tasks по довжинi name (за спаданням).");
			Console.WriteLine();

			Console.WriteLine($"UserId | FirstName {"|",2} Tasks");
			foreach (var user in orderedUsers)
			{
				Console.WriteLine($"{user.Id,6} | {user.FirstName,-10} | Tasks:");
				foreach (var task in user.Tasks)
				{
					Console.WriteLine($"{"|",8} {"|",12} {task.Id,3} | {task.Name}");
				}
			}
		}

		public static void PrintTask6(User_LatestProject_TasksCount_NotFinishedTasksCount_LongestDurationTask lastProject_tasks)
		{
			Console.WriteLine("Task 6:");
			Console.WriteLine("Отримати наступну структуру (передати Id користувача в параметри):");

			Console.WriteLine($"User {lastProject_tasks.User.Id}");
			Console.WriteLine($"Останнiй проект користувача(за датою створення) {lastProject_tasks.LatestProject.Id}");
			Console.WriteLine($"Загальна кiлькiсть таскiв пiд останнiм проектом {lastProject_tasks.LatestProjectTasksCount}");
			Console.WriteLine($"Загальна кiлькiсть незавершених або скасованих таскiв для користувача {lastProject_tasks.UsersNotFinishedTasksCount}");
			Console.WriteLine($"Найтривалiший таск користувача за датою {lastProject_tasks.UsersLongestDurationTask.Id}");
		}

		public static void PrintTask7(IEnumerable<Project_TwoTasks_TeamMembersCount> project_tasks_membersCount)
		{
			Console.WriteLine("Task 7:");
			Console.WriteLine("Отримати таку структуру:");
			Console.WriteLine($"Проект");
			Console.WriteLine($"Найдовший таск проекту (за описом)");
			Console.WriteLine($"Найкоротший таск проекту (по iменi)");
			Console.WriteLine($"Загальна кiлькiсть користувачiв в командi проекту, де або опис проекту >20 символiв, або кiлькiсть таскiв <3");

			Console.WriteLine($"Project | TaskByDescription | TaskByName | UsersCount");
			foreach (var project in project_tasks_membersCount)
			{
				Console.WriteLine($"{project.Project.Id,7} | {project.LongestDescriptionTask?.Id,-17} | {project.ShortestNameTask?.Id,-10} | {project.TeamMembersCount}");
			}
		}
	}
}
