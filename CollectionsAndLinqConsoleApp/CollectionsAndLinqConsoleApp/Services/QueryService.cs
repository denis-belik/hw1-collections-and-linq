﻿using CollectionsAndLinqConsoleApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CollectionsAndLinqConsoleApp.Models.ResultModels;

namespace CollectionsAndLinqConsoleApp
{
	class QueryService
	{
		private readonly HttpService _httpService;

		public QueryService(HttpService httpService)
		{
			_httpService = httpService;
		}

		// 1. Отримати кількість тасків у проекті конкретного користувача(по id) (словник, де key буде проект, а value кількість тасків).
		public async Task<Dictionary<Project, int>> GetTasksCountByProjectAuthorId(int authorId)
		{
			var projects = await _httpService.GetCollection<Project>("Projects");
			var tasks = await _httpService.GetCollection<Models.Task>("Tasks");

			return projects
				.Where(project => project.AuthorId == authorId)
				.GroupJoin(
					tasks,
					project => project.Id,
					task => task.ProjectId,
					(project, taskGroup) => new Project(project, taskGroup))
				.ToDictionary(project => project, project => project.Tasks.Count());
		}


		// 2. Отримати список тасків, призначених для конкретного користувача (по id), де name таска <45 символів (колекція з тасків).
		public async Task<IEnumerable<Models.Task>> GetTasksWithNameLengthLess45ByPerformerId(int performerId)
		{
			var tasks = await _httpService.GetCollection<Models.Task>("Tasks");
			var performer = await _httpService.GetEntity<User>($"Users/{performerId}");

			return tasks
				.Where(task => task.PerformerId == performerId && task.Name.Length < 45)
				.Join(
					new List<User>() { performer },
					task => task.PerformerId,
					performer => performer.Id,
					(task, performer) => new Models.Task(task, performer));
		}


		// 3. Отримати список (id, name) з колекції тасків, які виконані (finished) в поточному (2020) році для конкретного користувача (по id).
		public async Task<IEnumerable<TaskId_Name>> GetTasksFinishedIn2020ByPerformerId(int performerId)
		{
			var tasks = await _httpService.GetCollection<Models.Task>("Tasks");

			return tasks
				.Where(task =>
					task.PerformerId == performerId &&
					task.State == TaskState.Finished &&
					task.FinishedAt.Year == 2020)
				.Select(task => new TaskId_Name { Id = task.Id, Name = task.Name });
		}


		// 4. Отримати список (id, ім'я команди і список користувачів) з колекції команд, учасники яких старші 10 років,
		// відсортованих за датою реєстрації користувача за спаданням, а також згрупованих по командах.
		public async Task<IEnumerable<TeamId_Name_Members>> GetTeamsWithMembersOlderThan10Years()
		{
			var teams = await _httpService.GetCollection<Team>("Teams");
			var users = await _httpService.GetCollection<User>("Users");

			return users
				.Join(
					teams,
					user => user.TeamId,
					team => team.Id,
					(user, team) => new User(user, team))
				.Where(user => DateTime.Now.Year - user.Birthday.Year > 10)
				.OrderByDescending(user => user.RegisteredAt)
				.GroupBy(
					user => user.Team,
					(team, users) => new TeamId_Name_Members
					{ 
						Id = team.Id, 
						Name = team.Name, 
						Members = users 
					});
		}

		public async Task<IEnumerable<TeamId_Name_Members>> GetTeamsWithMembersOlderThan10YearsEXTRA_VARIANT()
		{
			var teams = await _httpService.GetCollection<Team>("Teams");
			var users = await _httpService.GetCollection<User>("Users");

			return teams
				.GroupJoin(
					users
						.Where(user => DateTime.Now.Year - user.Birthday.Year > 10)
						.OrderByDescending(user => user.RegisteredAt),
					team => team.Id,
					user => user.TeamId,
					(team, members) => new Team(team, members))
				.Where(team => team.Members.Any())
				.Select(team => new TeamId_Name_Members
				{
					Id = team.Id,
					Name = team.Name,
					Members = users
				});
		}


		// 5. Отримати список користувачів за алфавітом first_name(по зростанню) з відсортованими tasks по довжині name(за спаданням).
		public async Task<IEnumerable<User>> GetUsersOrderedByFirstNameWithTasksOrderedByNameLength()
		{
			var users = await _httpService.GetCollection<User>("Users");
			var tasks = await _httpService.GetCollection<Models.Task>("Tasks");

			return users
				.GroupJoin(
					tasks,
					user => user.Id,
					task => task.PerformerId,
					(user, taskGroup) => new User(user, taskGroup.OrderByDescending(task => task.Name.Length)))
				.OrderBy(user => user.FirstName);
		}


		// 6. Отримати наступну структуру (передати Id користувача в параметри):
		// User
		// Останній проект користувача (за датою створення)
		// Загальна кількість тасків під останнім проектом
		// Загальна кількість незавершених або скасованих тасків для користувача
		// Найтриваліший таск користувача за датою (найраніше створений - найпізніше закінчений)
		public async Task<User_LatestProject_TasksCount_NotFinishedTasksCount_LongestDurationTask> 
			GetUsersLatestProjectWithTasksCount_NotFinishedTasksCount_LongestDurationTask(int userId)
		{

			var user = await _httpService.GetEntity<User>($"Users/{userId}");
			var projects = await _httpService.GetCollection<Project>("Projects");
			var tasks = await _httpService.GetCollection<Models.Task>("Tasks");


			return new List<User> { user }
				.GroupJoin(
					projects,
					user => user.Id,
					project => project.AuthorId,
					(user, projectGroup) =>
						new User(user, projectGroup
										.OrderByDescending(project => project.CreatedAt)
										.GroupJoin(
											tasks,
											project => project.Id,
											task => task.ProjectId,
											(project, taskGroup) => new Project(project, taskGroup))))
				.GroupJoin(
					tasks,
					user => user.Id,
					task => task.PerformerId,
					(user, taskGroup) =>
						new User(user, taskGroup) { Projects = user.Projects })
				.Select(user => new User_LatestProject_TasksCount_NotFinishedTasksCount_LongestDurationTask
				{
					User = user,
					LatestProject = user.Projects.FirstOrDefault(),
					LatestProjectTasksCount = user.Projects.FirstOrDefault()?.Tasks.Count(),
					UsersNotFinishedTasksCount = user.Tasks.Where(task => task.State != TaskState.Finished).Count(),
					UsersLongestDurationTask = user.Tasks.OrderByDescending(task => task.FinishedAt - task.CreatedAt).FirstOrDefault()
				})
				.FirstOrDefault();
		}


		// 7. Отримати таку структуру:
		// Проект
		// Найдовший таск проекту(за описом)
		// Найкоротший таск проекту(по імені)
		// Загальна кількість користувачів в команді проекту, де або опис проекту >20 символів, або кількість тасків <3
		public async Task<IEnumerable<Project_TwoTasks_TeamMembersCount>> 
			GetProjectsWithLongestDescriptionAndShortestNameTasksWithTeamMembersCount()
		{
			var projects = await _httpService.GetCollection<Project>("Projects");
			var tasks = await _httpService.GetCollection<Models.Task>("Tasks");
			var teams = await _httpService.GetCollection<Team>("Teams");
			var users = await _httpService.GetCollection<User>("Users");

			return
				projects
				.GroupJoin(
					tasks,
					project => project.Id,
					task => task.ProjectId,
					(project, taskGroup) => new Project(project, taskGroup))
				.Join(
					teams.GroupJoin(
						users,
						team => team.Id,
						user => user.TeamId,
						(team, userGroup) => new Team(team, userGroup)),
					project => project.TeamId,
					team => team.Id,
					(project, team) => new Project(project, team) { Tasks = project.Tasks })
				.Select(project => new Project_TwoTasks_TeamMembersCount
				{
					Project = project,

					LongestDescriptionTask =
						project.Tasks.FirstOrDefault(
										task => task.Description.Length ==
												project.Tasks.Max(task => task.Description.Length)),

					ShortestNameTask =
						project.Tasks.FirstOrDefault(
										task => task.Name.Length ==
												project.Tasks.Min(task => task.Name.Length)),

					TeamMembersCount =
						project.Description.Length > 20 || project.Tasks.Count() < 3 ?
					    project.Team.Members.Count() :
						0
				});
		}
	}
}
