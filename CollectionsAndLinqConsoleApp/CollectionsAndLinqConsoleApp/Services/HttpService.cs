﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace CollectionsAndLinqConsoleApp
{
	public class HttpService
	{
		public const string ApiUrl = "https://bsa20.azurewebsites.net/api/";
		public HttpClient httpClient = new HttpClient();

		public async Task<List<T>> GetCollection<T>(string route)
		{
			var response = await httpClient.GetAsync(ApiUrl + route);
			response.EnsureSuccessStatusCode();

			string contentString = await response.Content.ReadAsStringAsync();

			var entities = JsonConvert.DeserializeObject<List<T>>(contentString);

			return entities;
		}

		public async Task<T> GetEntity<T>(string route)
		{
			var response = await httpClient.GetAsync(ApiUrl + route);
			response.EnsureSuccessStatusCode();

			string contentString = await response.Content.ReadAsStringAsync();

			var entity = JsonConvert.DeserializeObject<T>(contentString);

			return entity;
		}
	}
}
