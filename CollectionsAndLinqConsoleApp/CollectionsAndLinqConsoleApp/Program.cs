﻿using CollectionsAndLinqConsoleApp.Services;
using System;
using System.Threading.Tasks;

namespace CollectionsAndLinqConsoleApp
{
	class Program
	{
		static QueryService _queryService = new QueryService(new HttpService());

		static async Task Main()
		{
			try
			{
				while(true)
				{
					Console.WriteLine("Start: [taskNumber] [entityId]");
					string input = Console.ReadLine().Trim();
					if (!int.TryParse(input[0..1], out int taskNumber))
					{
						throw new ArgumentException("Incorrect taskNumber");
					}
					if (!int.TryParse(input[2..input.Length], out int entityId))
					{
						throw new ArgumentException("Incorrect entityId number");
					}
					Console.Clear();
					Console.WriteLine($"Task {taskNumber}, EntityId {entityId}");
					switch (taskNumber)
					{
						case 1:
							var projectTaskCount = await _queryService.GetTasksCountByProjectAuthorId(entityId);
							ConsoleService.PrintTask1(projectTaskCount);
							break;

						case 2:
							var tasksByNameLength = await _queryService.GetTasksWithNameLengthLess45ByPerformerId(entityId);
							ConsoleService.PrintTask2(tasksByNameLength);
							break;

						case 3:
							var tasksFinishedIn2020 = await _queryService.GetTasksFinishedIn2020ByPerformerId(entityId);
							ConsoleService.PrintTask3(tasksFinishedIn2020);
							break;

						case 4:
							var teamsWithMembersOlderThan10Years = await _queryService.GetTeamsWithMembersOlderThan10Years();
							ConsoleService.PrintTask4(teamsWithMembersOlderThan10Years);
							break;

						case 5:
							var orderedUsers = await _queryService.GetUsersOrderedByFirstNameWithTasksOrderedByNameLength();
							ConsoleService.PrintTask5(orderedUsers);
							break;

						case 6:
							var lastProject_tasks = await _queryService.GetUsersLatestProjectWithTasksCount_NotFinishedTasksCount_LongestDurationTask(entityId);
							ConsoleService.PrintTask6(lastProject_tasks);
							break;

						case 7:
							var project_tasks_membersCount = await _queryService.GetProjectsWithLongestDescriptionAndShortestNameTasksWithTeamMembersCount();
							ConsoleService.PrintTask7(project_tasks_membersCount);
							break;
						default:
							Console.WriteLine("Incorrect taskNumber.");
							break;
					}
				}			
			}
			catch(Exception ex)
			{
				Console.WriteLine(ex.Message);
				await Main();
			}		
		}
	}
}
